/*

*/

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

// iterative
// const insertNode = (head, value, index) => {
//   // for when inserting at head
//   if (index === 0) {
//     const newH = new Node(value);
//     newH.next = head;
//     return newH;
//   }

//   let curr = head,
//     count = 0;

//   while (curr !== null) {
//     if (count === index - 1) {
//       const next = curr.next;
//       curr.next = new Node(value);
//       curr.next.next = next;
//     }
//     count += 1;
//     curr = curr.next;
//   }

//   return head;
// };

// recursive
const insertNode = (head, value, index, count = 0) => {
  if (head === null) {
    return;
  }

  if (index === 0) {
    const newH = new Node(value);
    newH.next = head;
    return head;
  }

  if (count === index - 1) {
    const temp = head.next;
    head.next = new Node(value);
    head.next.next = temp;
    return head;
  }

  insertNode(head.next, value, index, count + 1);
  return head;
};
