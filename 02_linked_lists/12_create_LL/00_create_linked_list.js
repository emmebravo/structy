/*
Write a function, createLinkedList, that takes in an array of values as an argument. The function should create a linked list containing each element of the array as the values of the nodes. The function should return the head of the linked list.
*/
class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

// iterative
// const createLinkedList = (values) => {
//   let dummy = new Node(null),
//     tail = dummy;

//   for (let val of values) {
//     tail.next = new Node(val);
//     tail = tail.next;
//   }

//   return dummy.next;
// };

// recursive 1; T: O(n^2)
// const createLinkedList = (values) => {
//   if (values.length === 0) {
//     return null;
//   }
//   const head = new Node(values[0]);
//   head.next = createLinkedList(values.slice(1));

//   return head;
// };

// recursive 2; T: O(n), S: O(n)
const createLinkedList = (values, idx = 0) => {
  if (idx === values.length) {
    return null;
  }
  const head = new Node(values[i]);
  head.next = createLinkedList(values, idx + 1);

  return head;
};
