/*
Write a function, linkedListValues, that takes in the head of a linked list as an argument. The function should return an array containing all values of the nodes in the linked list.

Hey. This is our first linked list problem, so you should be liberal with watching the Approach and Walkthrough. Be productive, not stubborn. -AZ
*/

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

// iterative, T: O(n), S: O(n)
// const linkedListValues = (head) => {
//   let curr = head,
//     ans = [];
//   while (curr !== null) {
//     ans.push(curr.val);
//     curr = curr.next;
//   }

//   return ans;
// };

// recursively, T: O(n), S: O(n)
const linkedListValues = (head) => {
  const ans = [];
  fillValues(head, ans);

  return ans;
};

const fillValues = (head, ans) => {
  if (head === null) {
    return;
  }
  ans.push(head.val);
  fillValues(head.next, ans);
};
