class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

const a = new Node('a');
const b = new Node('b');
const c = new Node('c');
const d = new Node('d');

a.next = b;
b.next = c;
c.next = d;

// const ll_value = (head) => {
//   let current = head;
//   const arr = [];

//   while (current) {
//     arr.push(current.value);
//     current = current.next;
//   }

//   return arr;
// };

const ll_value = (head) => {
  const ans = [];
  ll_val_rec(head, ans);
  return ans;
};

const ll_val_rec = (head, ans) => {
  if (head === null) {
    return;
  }
  ans.push(head.value);
  ll_val_rec(head.next, ans);
};

console.log(ll_value(a));
