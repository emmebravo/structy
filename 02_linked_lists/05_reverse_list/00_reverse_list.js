/*
Write a function, reverseList, that takes in the head of a linked list as an argument. The function should reverse the order of the nodes in the linked list in-place and return the new head of the reversed linked list.
*/

// iterative
// const reverseList = (head) => {
//   let curr = head,
//     prev = null,
//     next = curr.next;

//   while (curr !== null) {
//     const next = curr.next;
//     curr.next = prev;
//     prev = curr;
//     curr = next;
//   }

//   return prev;
// };

// recursive
const reverseList = (head, prev = null) => {
  if (head === null) {
    return prev;
  }
  const next = head.next;
  head.next = prev;
  return reverseList(next, head);
};
