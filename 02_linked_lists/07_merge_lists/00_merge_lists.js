/*
Write a function, mergeLists, that takes in the head of two sorted linked lists as arguments. The function should merge the two lists together into single sorted linked list. The function should return the head of the merged linked list.

Do this in-place, by mutating the original Nodes.

You may assume that both input lists are non-empty and contain increasing sorted numbers.
*/

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}

// iterative
// const mergeLists = (head1, head2) => {
//   let dummy = new Node(0, null),
//     curr = dummy;

//   while (head1 && head2) {
//     if (head1.val < head2.val) {
//       curr.next = head1;
//       head1 = head1.next;
//     } else {
//       curr.next = head2;
//       head2 = head2.next;
//     }
//     curr = curr.next;
//   }

//   curr.next = head1 || head2;
//   return dummy.next;
// };

// recursive
const mergeLists = (head1, head2) => {
  if (head1 === null && head2 === null) {
    return null;
  }
  if (head1 === null) {
    return head2;
  }
  if (head2 === null) {
    return head1;
  }

  if (head1.val < head2.val) {
    const next = head1.next;
    head1.next = mergeLists(next, head2);
    return head1;
  } else {
    const next = head2.next;
    head2.next = mergeLists(head1, next);
    return head2;
  }
};
