/*
Write a function, zipperLists, that takes in the head of two linked lists as arguments. The function should zipper the two lists together into single linked list by alternating nodes. If one of the linked lists is longer than the other, the resulting list should terminate with the remaining nodes. The function should return the head of the zippered linked list.

Do this in-place, by mutating the original Nodes.

You may assume that both input lists are non-empty.
*/
// iterative
// const zipperLists = (head1, head2) => {
//   let count = 0,
//     head = head1,
//     tail = head,
//     c1 = head1.next,
//     c2 = head2;

//   while (curr1 && curr2) {
//     if (count % 2 === 0) {
//       tail.next = c2;
//       c2 = c2.next;
//     } else {
//       tail.next = c1;
//       c1 = c1.next;
//     }
//     tail = tail.next;
//     count++;
//   }

//   tail.next = c1 || c2;
//   return head;
// };

// recursive
const zipperLists = (head1, head2) => {
  if (head1 === null && head2 === null) {
    return null;
  }
  if (head1 === null) {
    return head2;
  }
  if (head2 === null) {
    return head1;
  }

  const next1 = head1.next;
  const next2 = head2.next;
  head1.next = head2;
  head2.next = zipperLists(next1, next2);

  return head1;
};
