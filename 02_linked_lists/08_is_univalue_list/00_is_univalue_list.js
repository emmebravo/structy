/*
Write a function, isUnivalueList, that takes in the head of a linked list as an argument. The function should return a boolean indicating whether or not the linked list contains exactly one unique value.

You may assume that the input list is non-empty.
*/

// iterative
// const isUnivalueList = (head) => {
//   let curr = head;

//   while (curr !== null) {
//     if (curr.val !== head.val) {
//       return false;
//     }
//     curr = curr.next;
//   }

//   return true;
// };

// recursive
const isUnivalueList = (head, prev = null) => {
  if (head === null) {
    return true;
  }

  if (prev === null || prev === head.val) {
    return isUnivalueList(head.next, head.val);
  } else {
    return false;
  }
};
