/*
Write a function, longestStreak, that takes in the head of a linked list as an argument. The function should return the length of the longest consecutive streak of the same value within the list.
*/

// iterative
const longestStreak = (head) => {
  let max_streak = 0,
    curr_streak = 0,
    prev = null,
    curr = head;

  while (curr !== null) {
    if (curr.val === prev) {
      curr_streak += 1;
    } else {
      curr_streak = 1;
    }

    if (curr_streak > max_streak) {
      max_streak = curr_streak;
    }

    prev = curr.val;
    curr = curr.next;
  }

  return max_streak;
};
