/*
Write a function, sumList, that takes in the head of a linked list containing numbers as an argument. The function should return the total sum of all values in the linked list.
*/

const a = new Node(2);
const b = new Node(8);
const c = new Node(3);
const d = new Node(-1);
const e = new Node(7);

a.next = b;
b.next = c;
c.next = d;
d.next = e;

// 2 -> 8 -> 3 -> -1 -> 7

// iteratively, T: O(n), S: O(n)
// const sumList = (head) => {
//   let curr = head,
//     sum = 0;
//   while (curr !== null) {
//     sum += curr.val;
//     curr = curr.next;
//   }

//   return sum;
// };

// recursively, T: O(n), S: O(n)
const sumList = (head) => {
  if (head === null) {
    return 0;
  }
  return head.val + sumList(head.next);
};

sumList(a); // 19
