/*
Write a function, getNodeValue, that takes in the head of a linked list and an index. The function should return the value of the linked list at the specified index.

If there is no node at the given index, then return null.
*/

// iterative
// const getNodeValue = (head, index) => {
//   let curr = head,
//     count = 0;

//   while (curr !== null) {
//     if (count === index) {
//       return curr.val;
//     }
//     count += 1;
//     curr = curr.next;
//   }

//   return null;
// };

// recursive
const getNodeValue = (head, index) => {
  if (head === null) {
    return head.val;
  }
  if (index === 0) {
    return head.val;
  }

  return getNodeValue(head.next, index - 1);
};
