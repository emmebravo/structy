/*
Write a function, anagrams, that takes in two strings as arguments. The function should return a boolean indicating whether or not the strings are anagrams. Anagrams are strings that contain the same characters, but in any order.
*/

const anagrams = (s1, s2) => {
  // todo
  if (s1.length !== s2.length) {
    return false;
  }

  const map = {};

  for (let letter of s1) {
    if (letter in map) {
      map[letter] += 1;
    } else {
      map[letter] = 1;
    }
  }

  for (let letter of s2) {
    if (map[letter] > 0) {
      map[letter] -= 1;
    } else {
      return false;
    }
  }

  return true;
};

anagrams('restful', 'fluster'); // -> true
anagrams('cats', 'tocs'); // -> false
anagrams('monkeyswrite', 'newyorktimes'); // -> true
anagrams('paper', 'reapa'); // -> false
anagrams('elbow', 'below'); // -> true
anagrams('tax', 'taxi'); // -> false
anagrams('taxi', 'tax'); // -> false
anagrams('night', 'thing'); // -> true
anagrams('abbc', 'aabc'); // -> false
anagrams('po', 'popp'); // -> false
anagrams('pp', 'oo'); // -> false
