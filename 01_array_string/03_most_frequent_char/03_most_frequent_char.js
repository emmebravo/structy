/*
Write a function, mostFrequentChar, that takes in a string as an argument. The function should return the most frequent character of the string. If there are ties, return the character that appears earlier in the string.

You can assume that the input string is non-empty.
*/

const mostFrequentChar = (s) => {
  const map = {};
  let mostChar = '',
    count = -Infinity;

  for (let letter of s) {
    if (letter in map) {
      map[letter] += 1;
    } else {
      map[letter] = 1;
    }
  }

  for (let key in map) {
    if (map[key] > count) {
      count = map[key];
      mostChar = key;
    }
  }

  /* alternative way to check for most frequent char
  let best = null;
  for (let char of s) {
    if (best === null || count[char] > count[best]) {
      best = char;
    }
  }
  */

  return mostChar;
};

mostFrequentChar('bookeeper'); // -> 'e'
mostFrequentChar('david'); // -> 'd'
mostFrequentChar('abby'); // -> 'b'
mostFrequentChar('mississippi'); // -> 'i'
mostFrequentChar('potato'); // -> 'o'
mostFrequentChar('eleventennine'); // -> 'e'
mostFrequentChar('riverbed'); // -> 'r'
