/*
Write a function, compress, that takes in a string as an argument. The function should return a compressed version of the string where consecutive occurrences of the same characters are compressed into the number of occurrences followed by the character. Single character occurrences should not be changed.

'aaa' compresses to '3a'
'cc' compresses to '2c'
't' should remain as 't'

You can assume that the input only contains alphabetic characters.
*/

const compress = (s) => {
  if (s.length <= 1) {
    return s;
  }

  let left = 0,
    right = 0,
    newStr = [];

  // <= bc in js you can compare idx with undefined
  while (right <= s.length) {
    if (s[left] !== s[right]) {
      let num = right - left;
      if (num > 1) {
        newStr.push(`${num}${s[left]}`); // keeps fcns inside if in O(1) time
      } else {
        newStr.push(s[left]); // keeps fcns inside if in O(1) time
      }
      left = right;
    }
    right++;
  }
  return newStr.join('');
};

compress('ccaaatsss'); // -> '2c3at3s'
compress('ssssbbz'); // -> '4s2bz'
compress('ppoppppp'); // -> '2po5p'
compress('nnneeeeeeeeeeeezz'); // -> '3n12e2z'
compress(
  'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy'
);
// -> '127y'
