# structy

## DSA Problems

- [Intro](/00_intro/)
- [Arrays & Strings](/01_array_string/)
- [Linked Lists](/02_linked_lists/)
- [Binary Search Trees (BTS)](/03_binary_trees)
- [Graphs](/04_graph/)
- [Dynamic Programming](/05_dynamic_programming/)
- [Stack](/06_stack/)
- [Exhaustive Recursion](/07_exhaustive_recursion/)
- [Mixed Recall](/08_mixed_recall/)

## Description

List of problems found on Structy. Videos of approach and walkthrough won't be shared. Code are my own answers; comments are Alvin's suggestions.

## Usage

This is repo is for learning purposes only. No infringement meant.

## License

Open source, use it to learn.
